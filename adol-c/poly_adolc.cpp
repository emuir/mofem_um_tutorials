#include <adolc/adolc.h>
#include <iostream>
#include <math.h>

using namespace std;

int main()
{
  adouble x;
  adouble y;
  //output
  double g;

  double active = 3;
  double dependent;

  //Tape 1
  trace_on(1);

  x <<= active; //active variable

  //Equation
  y = 3.0*x*x;

  y >>= dependent;

  trace_off();

  cout << "3x^2 when x=3: " << dependent << endl;

  //Use tape 1
  int err = gradient(1,1,&active,&g);

  cout << "dy/dx @ x=3: " << g << endl;
}
